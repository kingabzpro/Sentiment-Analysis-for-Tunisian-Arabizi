# Sentiment Analysis for Tunisian Arabizi

I will be using different model from Logistic regression to BERT transformer and pretrained hugging face model. 

## Introduction

On social media, Arabic speakers tend to express themselves in their own local dialect. To do so, Tunisians use ‘Tunisian Arabizi’, where the Latin alphabet is supplemented with numbers. However, annotated datasets for Arabizi are limited; in fact, this challenge uses the only known Tunisian Arabizi dataset in existence.

Sentiment analysis relies on multiple word senses and cultural knowledge, and can be influenced by age, gender and socio-economic status.For this task, we have collected and annotated sentences from different social media platforms. The objective of this challenge is to, given a sentence, classify whether the sentence is of positive, negative, or neutral sentiment. For messages conveying both a positive and negative sentiment, whichever is the stronger sentiment should be chosen. Predict if the text would be considered positive, negative, or neutral (for an average user). This is a binary task.

Such solutions could be used by banking, insurance companies, or social media influencers to better understand and interpret a product’s audience and their reactions.

This competition is one of five NLP challenges we will be hosting on Zindi as part of AI4D’s ongoing African language NLP project, and is a continuation of the African language dataset challenges we hosted earlier this year. You can read more about the work [here](https://zindi.medium.com/zindi-and-ai4d-build-language-datasets-for-african-nlp-34a4d0ea129).

## Datasets

The solution must use publicly-available, open-source packages only. Your models should not use any of the metadata provided.

You may use only the datasets provided for this competition. Automated machine learning tools such as automl are not permitted.

If the challenge is a computer vision challenge, image metadata (Image size, aspect ratio, pixel count, etc) may not be used in your submission.

You may use pretrained models as long as they are openly available to everyone.

The data used in this competition is the sole property of Zindi and the competition host. You may not transmit, duplicate, publish, redistribute or otherwise provide or make available any competition data to any party not participating in the Competition (this includes uploading the data to any public site such as Kaggle or GitHub). You may upload, store and work with the data on any cloud platform such as Google Colab, AWS or similar, as long as 1) the data remains private and 2) doing so does not contravene Zindi’s rules of use.

You must notify Zindi immediately upon learning of any unauthorised transmission of or unauthorised access to the competition data, and work with Zindi to rectify any unauthorised transmission or access.

Your solution must not infringe the rights of any third party and you must be legally entitled to assign ownership of all rights of copyright in and to the winning solution code to Zindi.

## Evaluation

Zindi maintains a **public leaderboard** and a **private leaderboard** for each competition. The Public Leaderboard includes approximately 50% of the test dataset. While the competition is open, the Public Leaderboard will rank the submitted solutions by the accuracy score they achieve. Upon close of the competition, the Private Leaderboard, which covers the other 50% of the test dataset, will be made public and will constitute the final ranking for the competition.

If you are in the top 20 at the time the leaderboard closes, we will email you to request your code. On receipt of email, you will have **48 hours to respond and submit your code following the submission guidelines detailed below.** Failure to respond will result in disqualification.

If your solution places **1st, 2nd, or 3rd on the final leaderboard**, you will be required to submit your winning solution code to us for verification, and you thereby agree to assign all worldwide rights of copyright in and to such winning solution to Zindi.

The metric for the classification tasks will be Accuracy.

For every row in the dataset, submission files should contain 2 columns: Comment_ID and Label (1:positive, -1: negative, 0: neutral).

Your submission file should look like this:

```python
ID          Label
273322      -1
937810       0
23123        1
```

## About iCompass

![img](https://zindpublic.blob.core.windows.net/public/uploads/image_attachment/image/568/7eef8822-9a4f-45f0-86c1-a3a01c794b7e.png)

iCompass is a Tunisian startup, created in July, 2019 and labelled startup act in August 2019. iCompass is specialized in the Artificial Intelligence field, and more particularly in the Natural Language Processing field. The particularity of iCompass is breaking the language barrier by developing systems that understand and interpret local dialects, especially African and Arab ones.

Read more about iCompass' work and their data [here](https://arxiv.org/abs/2004.14303).

